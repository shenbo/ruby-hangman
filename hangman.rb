require 'set'
require_relative 'hangman_controller.rb'
require_relative 'hangman_game.rb'
require_relative 'hangman_viewer.rb'

class Hangman
  NO_OF_LIVES = 26

  def initialize
    @model = HangmanGame.new(NO_OF_LIVES)
    @view = HangmanViewer.new
    @controller = HangmanController.new(@model, @view)
  end

  def run
    @view.welcome_message(@model.answer_status)

    until @model.won? || @model.lost?
      @controller.run_round
    end

    @view.end_message(@model.won?, @model.lost?, @model.answer)
  end
end

hangman = Hangman.new
hangman.run
