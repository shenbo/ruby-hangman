# Controller of a Hangman Game
class HangmanController
  ONE_UPPERCASE_LETTER = /^[A-Z]$/

  def initialize(model, view)
    @model = model
    @view = view
  end

  def run_round
    input = user_input

    @model.add_guessed_letter(input)

    @model.decrease_life unless @model.correct_guess?(input)

    @view.display_round_info(@model.correct_guess?(input), @model)
  end

  def user_input
    loop do
      input = @view.user_input
      error_message = validate_input(input)
      return input if error_message.nil?
      @view.display_input_error_message(error_message)
    end
  end

  def validate_input(input)
    return 'Please enter one letter only, no numbers or other characters' unless input =~ ONE_UPPERCASE_LETTER
    return 'You have guessed this letter already' if @model.guessed_already?(input)
  end
end
