# Model of a Hangman Game
class HangmanGame
  attr_reader :answer, :lives_remain, :guessed_letters

  UNREVEALED_LETTER = '*'.freeze

  def initialize(no_of_lives)
    @answer = create_answer
    @lives_remain = no_of_lives
    @guessed_letters = Set.new
  end

  def create_answer
    answer_length = rand(1..10)
    (0...answer_length).map { ('A'..'Z').to_a.sample }.join
  end

  def correct_guess?(letter)
    @answer.include?(letter)
  end

  def guessed_already?(letter)
    @guessed_letters.include?(letter)
  end

  def add_guessed_letter(new_letter)
    @guessed_letters << new_letter
  end

  def decrease_life
    @lives_remain -= 1
  end

  def won?
    answer_status == @answer
  end

  def lost?
    @lives_remain <= 0
  end

  def answer_status
    @answer.chars.map do |c|
      @guessed_letters.include?(c) ? c : UNREVEALED_LETTER
    end.join
  end

  def guessed_letters_string
    @guessed_letters.to_a.join
  end
end
