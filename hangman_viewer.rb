# Console view of Hangman Game
class HangmanViewer
  def welcome_message(answer_status)
    puts 'Welcome to the Hangman game!'
    puts "There are #{answer_status.length} letters in the word for this game!"
    puts answer_status
    puts 'Enter your letter:'
  end

  def display_round_info(is_correct_guess, game_model)
    puts game_model.answer_status
    puts is_correct_guess ? 'Correct guess' : 'Wrong guess!'
    puts "The letters you have guessed are: #{game_model.guessed_letters_string}"
    puts "You have #{game_model.lives_remain} lives left!"
  end

  def display_input_error_message(error_message)
    puts error_message
  end

  def end_message(won, lost, answer)
    puts 'You won!' if won
    puts "You lost, the correct answer is #{answer}" if lost
  end

  def user_input
    gets.chomp.upcase
  end
end
